﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Syncfusion.SfChart.XForms;
using Xamarin.Forms;

namespace SfChartDisposedRepro
{
    public class App : Application
    {
        ContentPage otherPage;
        ContentPage chartPage;
        public App()
        {
            var chart = new SfChart
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                PrimaryAxis = new CategoryAxis(),
                SecondaryAxis = new NumericalAxis()
            };

			var datasrc = new List<int> { 1, 1, 2, 3, 5, 8, 13, 21, 34 };

			var data = datasrc.Select(x => new ChartDataPoint(x.ToString(), x));
			chart.Series.Add(new ColumnSeries { ItemsSource = data });
			var chartStack = new StackLayout { VerticalOptions = LayoutOptions.FillAndExpand };
            var chartPageButton = new Button
            {
				Text = "Switch",
                VerticalOptions = LayoutOptions.End,
                Command = new Command(() => MainPage = otherPage)
            };
            chartStack.Children.Add(chart);
            chartStack.Children.Add(chartPageButton);

            var otherLabel = new Label
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
                Text = "test"
            };
			var otherStack = new StackLayout() { VerticalOptions = LayoutOptions.FillAndExpand };
            var otherPageButton = new Button
            {
				Text = "Switch",
                VerticalOptions = LayoutOptions.End,
                Command = new Command(() => MainPage = chartPage)
            };
            otherStack.Children.Add(otherLabel);
            otherStack.Children.Add(otherPageButton);

            chartPage = new ContentPage { Content = chartStack };
            otherPage = new ContentPage { Content = otherStack };
            MainPage = otherPage;
        }
    }
}
